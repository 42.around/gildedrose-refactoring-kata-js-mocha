# Gilded Rose Refactoring Kata
[![coverage report](https://gitlab.com/42.around/gildedrose-refactoring-kata-js-mocha/badges/js-mocha/coverage.svg)](https://gitlab.com/42.around/gildedrose-refactoring-kata-js-mocha/-/commits/js-mocha)

Gilded Rose kata solved using Node.js(ES6).

Patterns used to refactor source code:
- Factory pattern to simplify creation of new objects.
- Template pattern to simplify implementation of different Item types

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Node.js and `npm` are required to use this project. You can find more information official websites, listed in [Built With section](#built-with);

### Installing

After cloning this project, simply run

```
npm install
```

And project should download all required dependencies. You can test if it installed correctly by [running the tests](#running-the-tests)

## Running the tests

You can execute all project tests by running
```
npm test
```

It runs [style test](#style-test), [unit test](#unit-test) and [file test](#file-test).

### Style test

Style test is responsible for maintaining consistent code style and ensuring that project is running without issues.

If you want to check code style only, run

```
npm run test-style
```


### Unit tests

Unit tests are required to determine whether individual units of code are fit for use. You can launch unit tests by running

```
npm run test-unit
```


### File tests

File tests are required to determine whether console application is working properly. You can launch file tests by running

```
npm run test-file
```


## Built With

* [Node.js](https://nodejs.org/) - Runtime environment
* [npm](https://www.npmjs.com/) - Dependency Management

## Authors

* **Tomas Markevičius** - *Initial work* - [Tomas Markevičius](https://gitlab.com/42.around)

## License

This project is licensed under the MIT license - see the [LICENSE.md](LICENSE.md) file for details
