const fs = require('fs');
const es = require('event-stream');

const ItemUpdateAndSaveWritable = require('./src/ItemUpdateAndSaveWritable');

const { argv } = process;
const DATA_FILES = argv.slice(2);

// eslint-disable-next-line no-use-before-define
DATA_FILES.forEach((file) => processFile(file));

function processFile(inputFile) {
  // eslint-disable-next-line no-console
  console.log(`processing file "${inputFile}"`);

  const input = fs.createReadStream(inputFile);
  const splitLines = es.split(null, null, { trailing: false });
  const output = new ItemUpdateAndSaveWritable(inputFile);
  input.pipe(splitLines).pipe(output);
}
