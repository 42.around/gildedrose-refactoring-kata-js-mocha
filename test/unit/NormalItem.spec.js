const { expect } = require('chai');
const subject = '../../src/NormalItem.js';
const NormalItem = require(subject);

const getChangeRate = (item) => {
  const { quality } = item;
  item.update();
  return item.quality - quality;
};

describe(`NormalItem class properties and behavior, ${subject}`, () => {
  const item = new NormalItem('Normal item', 3, 10);

  it('should have "SellIn" and "Quality" properties', () => {
    expect(item).to.have.property('sellIn');
    expect(item).to.have.property('quality');
  });

  it('should decrease "SellIn" and "Quality" properties every day', () => {
    const { sellIn, quality } = item;

    item.update();

    expect(item.sellIn).to.be.below(sellIn);
    expect(item.quality).to.be.below(quality);
  });

  it('expired items "Quality" should change twice as fast', () => {
    expect(item.sellIn).to.be.above(0, 'item shouldn\'t be expired!');
    const changeRate = getChangeRate(item);

    while (item.sellIn > 0) {
      item.update();
    }

    const changeRateExpired = getChangeRate(item);
    expect(changeRateExpired).to.be.equal(2 * changeRate);
  });

  it('"Quality" should never be below 0 or above 50', () => {
    const min = 0;
    const max = 50;

    while (item.quality > min && item.quality < max) {
      item.update();
    }
    item.update();

    expect(item.quality).to.be.at.least(min);
    expect(item.quality).to.be.at.most(max);
  });
});
