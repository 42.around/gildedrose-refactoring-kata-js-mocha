const { expect } = require('chai');
const subject = '../../src/Sulfuras.js';
const Sulfuras = require(subject);

describe(`Sulfuras class properties and behavior, ${subject}`, () => {
  const item = new Sulfuras('Sulfuras, Hand of Ragnaros', 0, 80);

  it('should have "SellIn" and "Quality" properties', () => {
    expect(item).to.have.property('sellIn');
    expect(item).to.have.property('quality');
  });

  it('should keep same "SellIn" and "Quality" properties every day', () => {
    const { sellIn, quality } = item;

    item.update();

    expect(item.sellIn).to.be.equal(sellIn);
    expect(item.quality).to.be.equal(quality);
  });
});
