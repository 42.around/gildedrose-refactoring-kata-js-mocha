const { expect } = require('chai');
const subject = '../../src/ConjuredItem.js';
const ConjuredItem = require(subject);
const NormalItem = require('../../src/NormalItem.js');

const getChangeRate = (item) => {
  const { quality } = item;
  item.update();
  return item.quality - quality;
};

describe(`ConjuredItem class properties and behavior, ${subject}`, () => {
  const initialQuality = 42;
  const item = new ConjuredItem('Conjured item', 3, initialQuality);
  const normalItem = new NormalItem('Normal item', 3, initialQuality);

  it('should have "SellIn" and "Quality" properties', () => {
    expect(item).to.have.property('sellIn');
    expect(item).to.have.property('quality');
  });

  it('should decrease "SellIn" and "Quality" properties every day', () => {
    const { sellIn, quality } = item;

    item.update();

    expect(item.sellIn).to.be.below(sellIn);
    expect(item.quality).to.be.below(quality);
  });

  it('expired items "Quality" should change twice as fast', () => {
    expect(item.sellIn).to.be.above(0, 'item shouldn\'t be expired!');
    const changeRate = getChangeRate(item);
    normalItem.update();

    while (item.sellIn > 0) {
      item.update();
      normalItem.update();
    }

    const changeRateExpired = getChangeRate(item);
    normalItem.update();

    expect(changeRateExpired).to.be.equal(2 * changeRate);
  });

  it('"Quality" should never be below 0 or above 50', () => {
    const min = 0;
    const max = 50;

    while (item.quality > min && item.quality < max) {
      item.update();
      normalItem.update();
    }
    item.update();
    normalItem.update();

    expect(item.quality).to.be.at.least(min);
    expect(item.quality).to.be.at.most(max);
  });

  it('should degrade twice as fast as normal items', () => {
    expect(initialQuality - item.quality).to.be
      .equal(2 * (initialQuality - normalItem.quality));
  });
});
