const { expect } = require('chai');
const subject = '../../src/Shop.js';
const Shop = require(subject);
const Item = require('../../src/Item.js');
const createItem = require('../../src/createItem.js');

// All generalized test items should be stored in this array
const INITIAL_ITEM_CONFIGURATIONS = [
  { name: 'Another generic item', sellIn: 0, quality: 42 },
  { name: 'Conjured generic item', sellIn: 3, quality: 10 },
  { name: 'Aged Brie', sellIn: 2, quality: 10 },
  { name: 'Sulfuras, Hand of Ragnaros', sellIn: 0, quality: 80 },
  { name: 'Generic item', sellIn: 3, quality: 10 },
];

describe(`Shop class, ${subject}`, () => {
  const shop = new Shop();
  const shopItems = [];

  before(() => {
    INITIAL_ITEM_CONFIGURATIONS.forEach(({ name, sellIn, quality }) => {
      shop.items.push(createItem(name, sellIn, quality));
      shopItems.push(createItem(name, sellIn, quality));
    });
  });

  it('shop items should be an array', () => {
    expect(shop.items).to.be.an('array');
  });

  it('all shop items should be instances of Item class', () => {
    shop.items.forEach((item) => {
      expect(item).to.be.an.instanceof(Item);
    });
  });

  it('all shop items should have "SellIn" property with number value', () => {
    shop.items.forEach((item) => {
      expect(item).to.have.property('sellIn');
      expect(item.sellIn).to.be.a('number');
    });
  });

  it('all shop items "Quality" property with number value', () => {
    shop.items.forEach((item) => {
      expect(item).to.have.property('quality');
      expect(item.sellIn).to.be.a('number');
    });
  });

  it('expected to update all shop items quality every day', () => {
    expect(shop.items).to.deep.equal(shopItems);

    shopItems.forEach((item) => item.update());
    shop.updateQuality();

    expect(shop.items).to.deep.equal(shopItems);
  });
});
