const { expect } = require('chai');
const subject = '../../src/BackstagePasses.js';
const BackstagePasses = require(subject);

const getChangeRate = (item) => {
  const { quality } = item;
  item.update();
  return item.quality - quality;
};

describe(`BackstagePasses class properties and behavior, ${subject}`, () => {
  const item = new BackstagePasses('Backstage passes to a TAFKAL80ETC concert', 13, 2);

  it('should have "SellIn" and "Quality" properties', () => {
    expect(item).to.have.property('sellIn');
    expect(item).to.have.property('quality');
  });

  it('should decrease "SellIn" and increase "Quality"', () => {
    const { sellIn, quality } = item;
    item.update();

    expect(item.sellIn).to.be.below(sellIn);
    expect(item.quality).to.be.above(quality);
  });

  it('should increase "Quality" by 2 when 10 or less days left', () => {
    expect(item.sellIn).to.be.above(10);

    while (item.sellIn > 10) {
      item.update();
    }

    expect(getChangeRate(item)).to.be.equal(2);
    expect(getChangeRate(item)).to.be.equal(2);
  });

  it('should increase "Quality" by 3 when 5 or less days left', () => {
    expect(item.sellIn).to.be.above(5);

    while (item.sellIn > 5) {
      item.update();
    }

    expect(getChangeRate(item)).to.be.equal(3);
    expect(getChangeRate(item)).to.be.equal(3);
  });

  it('should drop "Quality" to 0 when expired', () => {
    expect(item.sellIn).to.be.above(0, 'item shouldn\'t be expired!');

    while (item.sellIn >= 0) {
      item.update();
    }

    expect(item.quality).to.be.equal(0);
  });
});
