const NormalItem = require('./NormalItem');

class ConjuredItem extends NormalItem {
  getDegradationRate() {
    return this.sellIn > 0 ? 2 : 4;
  }
}

module.exports = ConjuredItem;
