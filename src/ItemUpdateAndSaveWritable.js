const { Writable } = require('stream');
const fs = require('fs');

const createItem = require('./createItem');
const Item = require('./Item');

function itemFromLine(line) {
  const parameters = line.split('#').map((parameter) => parameter.trim());
  if (parameters.length !== 3) return null;

  return createItem(
    parameters[0], Number(parameters[1]), Number(parameters[2]),
  );
}

function itemToLine({ name, sellIn, quality }) {
  return `   ${name}  #  ${sellIn}  #  ${quality}  \n`;
}

class ItemUpdateAndSaveWritable extends Writable {
  constructor(baseName, options) {
    super(options);

    this.baseName = baseName;
    this.outputStreams = {};
  }

  getOutputStream(itemType) {
    if (!(itemType in this.outputStreams)) {
      this.outputStreams[itemType] = fs.createWriteStream(
        `${this.baseName}_${itemType}.out`,
      );
    }

    return this.outputStreams[itemType];
  }

  // eslint-disable-next-line no-underscore-dangle
  _write(chunk, encoding, callback) {
    const line = chunk.toString();
    const item = itemFromLine(line);

    if (item instanceof Item) {
      item.update();
      const outputStream = this.getOutputStream(item.constructor.name);
      outputStream.write(itemToLine(item));
      callback();
    } else {
      callback(new Error(`Failed to parse line content: "${line}"`));
    }
  }
}

module.exports = ItemUpdateAndSaveWritable;
