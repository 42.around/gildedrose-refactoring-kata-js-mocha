const Item = require('./Item');

class NormalItem extends Item {
  getDegradationRate() {
    return this.sellIn > 0 ? 1 : 2;
  }

  update() {
    const quality = this.quality - this.getDegradationRate();
    // NOTE: 0 <= Quality <= 50 for normal items
    this.quality = Math.min(50, Math.max(0, quality));

    this.sellIn -= 1;
  }
}

module.exports = NormalItem;
