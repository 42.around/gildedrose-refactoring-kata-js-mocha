const AgedBrie = require('./AgedBrie');
const BackstagePass = require('./BackstagePasses');
const ConjuredItem = require('./ConjuredItem');
const NormalItem = require('./NormalItem');
const Sulfuras = require('./Sulfuras');

// Factory pattern
const createItem = (name, sellIn, quality) => {
  if (name.startsWith('Conjured')) {
    return new ConjuredItem(name, sellIn, quality);
  }

  switch (name) {
    case 'Backstage passes to a TAFKAL80ETC concert':
      return new BackstagePass(name, sellIn, quality);

    case 'Sulfuras, Hand of Ragnaros':
      return new Sulfuras(name, sellIn, quality);

    case 'Aged Brie':
      return new AgedBrie(name, sellIn, quality);

    default:
      return new NormalItem(name, sellIn, quality);
  }
};

module.exports = createItem;
