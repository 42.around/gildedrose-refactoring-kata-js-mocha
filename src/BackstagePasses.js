const NormalItem = require('./NormalItem');

class BackstagePasses extends NormalItem {
  getDegradationRate() {
    if (this.sellIn > 10) return -1;
    if (this.sellIn > 5) return -2;
    if (this.sellIn > 0) return -3;
    return this.quality; // NOTE: Degrade to 0 if concert already happened
  }
}

module.exports = BackstagePasses;
