const NormalItem = require('./NormalItem');

class AgedBrie extends NormalItem {
  getDegradationRate() {
    return this.sellIn > 0 ? -1 : -2;
  }
}

module.exports = AgedBrie;
